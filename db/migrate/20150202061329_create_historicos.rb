class CreateHistoricos < ActiveRecord::Migration
  def change
    create_table :historicos do |t|
      t.text :nota
      t.string :usuario_encargado
      t.references :ticket

      t.timestamps
    end
    add_index :historicos, :ticket_id
  end
end
