class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :titulo
      t.text :descripcion
      t.string :creador
      t.string :tecnico_asignado
      t.string :prioridad
      t.string :estado

      t.timestamps
    end
  end
end
