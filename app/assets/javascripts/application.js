// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require_tree .

$(function(){

  function getPosition(name){
    if (!name) return -1;
    var object = name.substring(0, name.lastIndexOf('[')); // extracts array[pos] from array[pos][key]
    var pos = object.substring(object.lastIndexOf('[')+1, object.lastIndexOf(']')); // extracts pos from array[pos]
    pos = parseInt(pos);
    if (isNaN(pos)) return -1;
    return pos;
  }

  function setPosition(name, position){
    object = name.substr(0, name.lastIndexOf('[')),
    field = name.substr(name.lastIndexOf('['));
    return object.substr(0, object.lastIndexOf('[')) + '[' + position + ']' + field;
  }

  var positions = $('.nested-forms :input').map(function(index, element){
    return getPosition($(element).attr('name'));
  });
  var lastPosition = -1;
  for (var i=0; i<positions.length; i++){
    if (positions[i] > lastPosition) lastPosition = positions[i];
  }

  $('.nested-forms').each(function(index, nestedForms){
    var $nestedForms = $(nestedForms),
        $tmp = $nestedForms.find('.nested-form-template'),
        $template = $tmp.clone(true);
    $tmp.remove();
    $nestedForms.find('.nested-form-add').click(function(event){
      event.preventDefault();
      var $nuevo = $template.clone(true);
      lastPosition++;
      // Eliminar indice del array (para que no sean iguales y solo envie uno)
      $nuevo.find(':input').each(function(index, input){
        var $input = $(input),
            name = $input.attr('name');
        if (name){
          $input.attr('name', setPosition(name, lastPosition));
        }
      });
      $(this).before($nuevo);
    });
  });
  $('.nested-forms').on('click', '.nested-form-remove', function(event){
    event.preventDefault();
    var $this = $(this),
        $nestedForm = $this.closest('.nested-form'),
        $idElement = $nestedForm.next();
    // Si tiene ID es que está guardado y tenemos que añadir el campo _destroy
    var name;
    if ($idElement.size() == 1 && (name = $idElement.attr('name')) && name.substr(-4) == '[id]'){
      var destroy = $idElement.clone();
      destroy.attr('name', name.substr(0, name.length-4) + '[_destroy]');
      destroy.val('1');
      $idElement.after(destroy);
    }
    $nestedForm.hide();
  });

});