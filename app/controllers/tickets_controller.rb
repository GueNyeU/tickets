class TicketsController < ApplicationController
  load_and_authorize_resource
  
  # GET /tickets
  # GET /tickets.json
  def index
    if params[:search]
      if current_user.role? :administrador
        @tickets = Ticket.search(params[:search]).order(params[:sort])
      elsif current_user.role? :paciente
        @tickets = Ticket.search(params[:search]).where(user_id: current_user).order(params[:sort])
      else
        @tickets = Ticket.search(params[:search]).where(tecnico_asignado: current_user).order(params[:sort])
      end
     else
       if current_user.role? :administrador
         @tickets = Ticket.find(:all, order: params[:sort])
       elsif current_user.role? :paciente
         @tickets = Ticket.where(user_id: current_user).order(params[:sort])
       else
         @tickets = Ticket.where(tecnico_asignado: current_user).order(params[:sort])
       end
    end

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tickets }
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
    @ticket = Ticket.find(params[:id])
    if !@ticket.tecnico_asignado.nil?
      @user = User.where(role: "tecnico").where(id: @ticket.tecnico_asignado)
      @user = @user.first.username
    end
    
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /tickets/new
  # GET /tickets/new.json
  def new
    @ticket = Ticket.new
    @ticket.historicos.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @ticket }
    end
  end

  # GET /tickets/1/edit
  def edit
    @ticket = Ticket.find(params[:id])
    @ticket.historicos.new
    @user = User.where(role: "tecnico")
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(params[:ticket])

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to @ticket, notice: 'Ticket creado.' }
        format.json { render json: @ticket, status: :created, location: @ticket }
      else
        format.html { render action: "new" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tickets/1
  # PUT /tickets/1.json
  def update
    @ticket = Ticket.find(params[:id])
    @user = User.all

    respond_to do |format|
      if @ticket.update_attributes(params[:ticket])
        format.html { redirect_to @ticket, notice: 'Ticket actualizado.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket = Ticket.find(params[:id])
    @ticket.destroy

    respond_to do |format|
      format.html { redirect_to tickets_url }
      format.json { head :no_content }
    end
  end
end
