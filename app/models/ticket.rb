class Ticket < ActiveRecord::Base
  has_many :historicos
  
  accepts_nested_attributes_for :historicos, :allow_destroy => true, :reject_if => :all_blank
  
  attr_accessible :creador, :descripcion, :estado, :prioridad, :tecnico_asignado, :titulo, :historicos_attributes, :user_id
  
  PRIORIDAD = %w[inmediata alta normal baja]
  REALIZADO = %w[inicio asignado en_proceso cerrado]
  
  def self.search(query)
     # where(:title, query) -> This would return an exact match of the query
     where("id like ?", "%#{query}%")
   end
end
